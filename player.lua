--- initialize quads
--- expensive call, initialize only once
look_down = love.graphics.newQuad(0, 0, 128, 48, 128, 192)
look_left = love.graphics.newQuad(0, 48, 128, 48, 128, 192)
look_right = love.graphics.newQuad(0, 96, 128, 48, 128, 192)
look_up = love.graphics.newQuad(0, 144, 128, 48, 128, 192)

--- load spritesheet of main character
--- initialize quad to down
player = love.graphics.newImage("assets/lyra.png")
pos = look_down
pos:setViewport(0, 0, 32, 48)

--- initialize position to center of screen
pos_x = love.graphics.getWidth() / 2
pos_y = love.graphics.getHeight() / 2

--- increment anim_delay every keypress until anim_delay == anim_max_delay
--- then display animation, reset to zero
--- do this or else animation moves too fast
anim_delay = 0
anim_max_delay = 8

function getWidth()
	x, y, w, h = pos:getViewport()
	if x < 3 * w then
		return x + w
	else
		return 0
	end
end

function down(dt)
	pos_y = pos_y + (dt * 110)
	if anim_delay == anim_max_delay then			
		look_down:setViewport(getWidth(), 0, 32, 48)
		pos = look_down
		anim_delay = 0
	elseif anim_delay < anim_max_delay then
		anim_delay = anim_delay + 1
	end
end

function up(dt)
	pos_y = pos_y - (dt * 110)
	if anim_delay == anim_max_delay then			
		look_up:setViewport(getWidth(), 144, 32, 48)
		pos = look_up
		anim_delay = 0
	elseif anim_delay < anim_max_delay then
	    anim_delay = anim_delay + 1
	end	
end

function left(dt)
	pos_x = pos_x - (dt * 110)
	if anim_delay == anim_max_delay then			
		look_left:setViewport(getWidth(), 48, 32, 48)
		pos = look_left
		anim_delay = 0
	elseif anim_delay < anim_max_delay then
	    anim_delay = anim_delay + 1
	end	
end

function right(dt)
	pos_x = pos_x + (dt * 110)
	if anim_delay == anim_max_delay then			
		look_right:setViewport(getWidth(), 96, 32, 48)		
		anim_delay = 0
		pos = look_right
	elseif anim_delay < anim_max_delay then
	    anim_delay = anim_delay + 1
	end	
end

function up_left(dt)
	pos_x = pos_x - (dt * 110)		
	pos_y = pos_y - (dt * 110)
	if anim_delay == anim_max_delay then			
		look_up:setViewport(getWidth(), 144, 32, 48)
		pos = look_up
		anim_delay = 0
	elseif anim_delay < anim_max_delay then
	    anim_delay = anim_delay + 1
	end	
end

function up_right(dt)
	pos_x = pos_x + (dt * 110)		
	pos_y = pos_y - (dt * 110)
	if anim_delay == anim_max_delay then			
		look_up:setViewport(getWidth(), 144, 32, 48)
		pos = look_up
		anim_delay = 0
	elseif anim_delay < anim_max_delay then
	    anim_delay = anim_delay + 1
	end	
end

function down_left(dt)
	pos_x = pos_x - (dt * 110)
	pos_y = pos_y + (dt * 110)
	if anim_delay == anim_max_delay then			
		look_down:setViewport(getWidth(), 0, 32, 48)
		pos = look_down
		anim_delay = 0
	elseif anim_delay < anim_max_delay then
	    anim_delay = anim_delay + 1
	end
end

function down_right(dt)
	pos_y = pos_y + (dt * 110)
	pos_x = pos_x + (dt * 110)		
	if anim_delay == anim_max_delay then			
		look_down:setViewport(getWidth(), 0, 32, 48)
		pos = look_down
		anim_delay = 0
	elseif anim_delay < anim_max_delay then
	    anim_delay = anim_delay + 1
	end
end
