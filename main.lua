require("player")
require("map")

function love.load()

end

function love.update(dt)

	if love.keyboard.isDown("down") and not love.keyboard.isDown("left", "right") then
		down(dt)
	end

	if love.keyboard.isDown("left") and not love.keyboard.isDown("up", "down") then
		left(dt)
	end

	if love.keyboard.isDown("right") and not love.keyboard.isDown("up", "down") then
		right(dt)		
	end

	if love.keyboard.isDown("up") and not love.keyboard.isDown("left", "right") then
		up(dt)	
	end

	if love.keyboard.isDown("right") and love.keyboard.isDown("down") then
		down_right(dt)
	end

	if love.keyboard.isDown("right") and love.keyboard.isDown("up") then
		up_right(dt)
	end

	if love.keyboard.isDown("left") and love.keyboard.isDown("down") then
		down_left(dt)
	end

	if love.keyboard.isDown("left") and love.keyboard.isDown("up") then
		up_left(dt)	
	end


end

function love.draw()
	love.graphics.draw(spriteBatch)
	love.graphics.draw(player, pos, pos_x, pos_y, 0, 1, 1, 0, 0)
end

function love.keypressed(key)
	if key == "escape" then
		love.event.quit()
	end
end